<?php
	class m_terima_tagihan extends CI_Model {
		function __construct() {
        	// Call the Model constructor
        	parent::__construct();
    	}

    function get_data_vendor($params){
    	$sql = "SELECT * FROM masterdata_ebi WHERE purchasing_document = $params GROUP BY purchasing_document";
    	$query = $this->db->query($sql);
    	if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result['vendor'];
        } else {
            return NULL;
        }
    }
    //
    function save_receivea_tagihan($user_id,$data){    	
    	$invoice_repeat=$this->cek_invoice_repeat($invoice_number)+1;
    	$sql = "INSERT INTO tanda_terima_invoice(purchasing_document, invoice_repeat,invoice_number,invoice_status,pic_loket) VALUES('$no_po','$invoice_repeat','$invoice_number','$invoice_status','$user_id')";
    	return $this->db->query($sql);
    }

    function save_tagihan($user_id,$data){ 
        $no_po = $data["no_po"];
        $invoice_status = $data["invoice_status"];
        
        if(!isset($data['invoice_number'])){
            $invoice_number='';
        }

        $role = $this->session->userdata('user_auth');

        $invoice_repeat=$this->cek_invoice_repeat($no_po,$role)+1;



        if(isset($data["invoice_status2"])){
            $invoice_status2=$data["invoice_status2"];
            $reason = $data["reason"];

            $sql = "INSERT INTO masterdata_invoice_log(purchasing_document, invoice_repeat,status_post,reason,pic_post,post) VALUES('$no_po','$invoice_repeat','$invoice_status2','$reason','$user_id','$role')";
        }
        else
            $sql = "INSERT INTO masterdata_invoice_log(purchasing_document, invoice_repeat,status_post,pic_post,post) VALUES('$no_po','$invoice_repeat','Accepted','$user_id','$role')";
        

        $this->db->query($sql);

        $sql = "SELECT purchasing_document FROM masterdata_invoice WHERE purchasing_document='$no_po' AND invoice_number=''";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $num_same = $result;
        } else {
            $num_same = 0;
        }

        if($num_same==0){
            $sql = "INSERT INTO masterdata_invoice(purchasing_document) VALUES($no_po)";
            return $this->db->query($sql);
        }
    }

    function cek_invoice_repeat($param1,$param2){
    	$sql = "SELECT invoice_number FROM masterdata_invoice_log WHERE purchasing_document='$param1' AND post='$param2'";
    	$query=$this->db->query($sql);
    	return $query->num_rows();
    }

}
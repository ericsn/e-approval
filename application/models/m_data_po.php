<?php
	class m_data_po extends CI_Model {
		function __construct() {
        	// Call the Model constructor
        	parent::__construct();
    	}

    	function get_termin_po($params){
    		$sql = "SELECT * FROM termin WHERE Purchasing_Document = ?";
    		$query = $this->db->query($sql, $params);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }
    	}

    	function GetTermin(){
    		$sql = "SELECT * FROM termin";
    		$query = $this->db->query($sql);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }
    	}
	}
?>
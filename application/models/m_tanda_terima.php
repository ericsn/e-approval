<?php
	class m_tanda_terima extends CI_Model {
		function __construct() {
        	// Call the Model constructor
        	parent::__construct();
    	}

    	function get_all_tanda_terima(){
    		$sql = "SELECT * FROM v_masterdata_invoice";

    		$query = $this->db->query($sql);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }
    	}

    	function get_all_data_invoice_po($id){
    		$sql = "SELECT * FROM v_tanda_terima_invoice WHERE purchasing_document='$id'";

    		$query = $this->db->query($sql);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }
    	}

    	function get_all_worklist($user_id){
    		$sql = "SELECT * FROM v_tanda_terima_invoice WHERE pic_loket='$user_id' OR pic_gr='$user_id' OR pic_unblocktermin='$user_id' OR pic_reviewdenda='$user_id' OR pic_pembayaran='$user_id'";

    		$query = $this->db->query($sql);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }
    	}

    	function get_all_id_po($id){
    		$sql = "SELECT inbound_delivery FROM masterdata_ebi WHERE purchasing_document='$id' AND inbound_delivery<>''";

    		$query = $this->db->query($sql);
    		if ($query->num_rows() > 0){
	            $result = $query->result_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return array();
	        }	
    	}

    	function check_selected_id($purchasing_document, $data,$data2){
    		$x=0;
    		for($i=$data;$i<=$data2;$i++){
    			$sql = "SELECT inbound_delivery FROM masterdata_ebi WHERE purchasing_document='$purchasing_document' AND inbound_delivery='$i'";
    			$query = $this->db->query($sql);
	    		if ($query->num_rows() > 0){
	    			$backdata[$x]=$i;
	    			$x++;
	    		}
    		}
    		return $backdata;
    	}

    	function get_all_invoice_data($purchasing_document,$invoice_number){
    		$sql = "SELECT * FROM masterdata_invoice WHERE purchasing_document='$purchasing_document' AND invoice_number='$invoice_number'";

    		$query = $this->db->query($sql);
	    	if ($query->num_rows() > 0) {
	            $result = $query->row_array();
	            $query->free_result();
	            return $result;
	        } else {
	            return NULL;
	        }	
	}
}
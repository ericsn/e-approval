<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


                <!-- TABEL DATA DENDA -->
                <table id="example2" class="table table-bordered table-striped" style="font-size:12px;">
                  <h4><b>TERMIN</b></h4>
                <thead>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Item</th>
                  <th>Termin</th>
                  <th>Inv. Percentage</th>
                  <th>Billing Value</th>
                  <th>Currency</th>
                  <th>Block Status</th>
                  <th>Billing Status</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $number_uncompleted_tasks=0;
                  $no=0;
                   if(isset($data_termin)){
                    foreach ($data_termin as $termin){
                      $no++;
                      if($termin["block_status"]=="Blocked for Invoice" || $termin["block_status"]=="Created")
                        echo "<tr style='padding-left:100px;'>";
                      else{
                        echo "<tr style='padding-left:100px;background-color:#e96666;color:white;'>";
                        $number_uncompleted_tasks++;
                      }
                       
                      echo "
                          <td>".$no."</td>
                          <td>".$termin["purchasing_document"]."</td>
                          <td>".$termin["item"]."</td>
                          <td>".$termin["termin"]."</td>
                          <td>".$termin["invoice_percentage"]."</td>
                          <td>".$termin["billing_value"]."</td>
                          <td>".$termin["currency"]."</td>
                          <td>".$termin["block_status"]."</td>
                          <td>".$termin["billing_status"]."</td>
                          </tr>
                          ";
                    }
                  }
                
                ?>

                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Item</th>
                  <th>Termin</th>
                  <th>Inv. Percentage</th>
                  <th>Billing Value</th>
                  <th>Currency</th>
                  <th>Block Status</th>
                  <th>Billing Status</th>
                </tr>
                </tfoot>
              </table>
              <!-- AKHIR TABEL DATA DENDA -->
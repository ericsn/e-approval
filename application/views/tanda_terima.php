<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    
    <?php  ?>

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="<?=site_url('tagihan')?>">Tagihan</a></li>
        <li class="active"><a href="<?=site_url('tanda_terima')?>">Tanda Terima</a></li>
      </ol>
    </section>
    <br>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      



      <div class="row">
              <div class="col-md-12">
             
                <div class="box">
            <div>
                  <div class="box-header">
                            <h3 class=box-title>Tanda Terima</h3>
                        </div>
                  
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                        
                <br>
                <?php
                  if(isset($data_tanda_terima)){ 
                    $this->load->view("data_tanda_terima");
                  }
                ?>


            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->

              </div>
       </div>


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

</body>
</html>

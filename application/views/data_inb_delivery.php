<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

                <!-- TABEL DATA INBOUND DELIVERY -->
                <table id="example1" class="table table-bordered table-striped" style="font-size:14px;">
                  <h4><b>INBOUND DELIVERY</b></h4>
                <thead>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Item</th>
                  <th>Material Desc.</th>
                  <th>Inb. Delivery</th>
                  <th>Plant</th>
                  <th>Stor. Loc.</th>
                  <th>PO Qty</th>
                  <th>Unit</th>
                  <th>Net Value</th>
                  <th>Currency</th>
                  <th>Delivery Date</th>
                  <th>Material Doc.</th>
                  <th>Posting Date</th>
                  <th>Doc. Date</th>
                  <th>Qty Terima</th>
                  <th>Unit</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $number_uncompleted_tasks=0;
                  $no=0;
                   if(isset($data_inb_deliv)){
                    foreach ($data_inb_deliv as $inb_deliv){
                      $no++;
                      if($inb_deliv["document_date"]!=NULL)
                        echo "<tr style='padding-left:100px;'>";
                      else{
                        echo "<tr style='padding-left:100px;background-color:#e96666;color:white;'>";
                        $number_uncompleted_tasks++;
                      }
                       
                      echo "
                          <td>".$no."</td>
                          <td>".$inb_deliv["purchasing_document"]."</td>
                          <td>".$inb_deliv["item"]."</td>
                          <td>".$inb_deliv["material_description"]."</td>
                          <td>".$inb_deliv["inb_delivery"]."</td>
                          <td>".$inb_deliv["plant"]."</td>
                          <td>".$inb_deliv["storage_location"]."</td>
                          <td>".$inb_deliv["po_qty"]."</td>
                          <td>".$inb_deliv["po_unit"]."</td>
                          <td>".$inb_deliv["net_value"]."</td>
                          <td>".$inb_deliv["currency"]."</td>
                          <td>".$inb_deliv["delivery_date"]."</td>
                          <td>".$inb_deliv["material_document"]."</td>
                          <td>".$inb_deliv["posting_date"]."</td>
                          <td>".$inb_deliv["document_date"]."</td>
                          <td>".$inb_deliv["qty_terima"]."</td>
                          <td>".$inb_deliv["unit_terima"]."</td>
                          </tr>
                          ";
                    }
                  }
                
                ?>

                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Item</th>
                  <th>Material Description</th>
                  <th>Inb. Delivery</th>
                  <th>Plant</th>
                  <th>Stor. Loc.</th>
                  <th>PO Qty</th>
                  <th>Unit</th>
                  <th>Net Value</th>
                  <th>Currency</th>
                  <th>Delivery Date</th>
                  <th>Material Doc.</th>
                  <th>Posting Date</th>
                  <th>Document Date</th>
                  <th>Qty Terima</th>
                  <th>Unit</th>
                </tr>
                </tfoot>
              </table>
              <!-- AKHIR TABEL DATA INBOUND DELIVERY -->
<?php
	echo "<div class='alert alert-danger alert-dismissible'>
            <button type=button class=close data-dismiss=alert aria-hidden=true>&times;</button>
            <h4><i class='icon fa fa-ban'></i> Alert!</h4>
            <br><br>You do not authorized to access this page.
            <br> Please contact our Administrator to get access through this page.
          </div>";
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    
    <?php  ?>

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="<?=site_url('dashboard')?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="<?=site_url('tagihan')?>">Tagihan</a></li>
        <li><a href="<?=site_url('penerimaan_tagihan')?>">Tanda Terima</a></li>
      </ol>
    </section>
    <br>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      



      <div class="row">
        <div class="col-md-12">
          <div class="box">

            <!-- START LINE | CALL SEARCH BAR -->
            <div class="box-body">
                <?php
                    echo "<div class=box-header>
                          <h3 class=box-title>Input Data Tagihan</h3>
                         </div>";
                    $this->load->view('search_po');
                ?>
            </div>
            <!-- END LINE | CALL SEARCH BAR -->

            </div>
              <?php
                if(isset($nama_vendor) && isset($no_po)){ 
                  echo "<div class=box> <div class=box-body>";
                  if($userdata["auth_2"]==1){
                    if($invoice_status=="Received"){

                        echo "<div style='font-size:30px'><a href=".site_url('penerimaan_tagihan')."><i class='fa fa-arrow-circle-left' title='Kembali'></i></a></div>";

                        if(!isset($invoice_number))
                          echo "<br><h4><b>Terima Tagihan</b></h4>";
                        else
                          echo "<br><h4><b>Tolak Tagihan</b></h4>";
                        
                        echo form_open('penerimaan_tagihan/submit_tagihan');
                        echo "<table style='font-size:14px'>
                        <tr>
                          <td>Nomor PO/SPK/Kontrak&nbsp&nbsp&nbsp</td>
                          <td> ";
                        $data = array('name'=>'no_po','class'=>'form-control','type'=>'text','value' => $no_po, 'readonly'=>TRUE, 'style'=>'margin-bottom:10px');
                        echo form_input($data)."</td></tr>";
                        
                        echo "<tr>
                                <td>Nama Vendor</td>
                                <td> ";
                        $data = array('name'=>'nama_vendor','class'=>'form-control','type'=>'text','value' => $nama_vendor, 'readonly'=>TRUE, 'style'=>'margin-bottom:10px');
                        echo form_input($data)."</td></tr>";

                        
                        echo "</table>";

                        if(!isset($invoice_number)){
                          $data = array('type' => 'submit','class'=> 'btn btn-success','name'=> 'proceed','value' => 'Receive','title' => 'Terima & Simpan');
                          echo form_submit($data);

                          $data = array('type' => 'submit','class'=> 'btn btn-danger','name'=> 'proceed','value' => 'Reject','title' => 'Tolak & Masukkan Alasan');
                          echo form_submit($data);
                        }

                        if(isset($invoice_status2) && $invoice_status2=="Rejected"){
                              
                              echo "<br><b>Reason Reject :</b><br><br>";
                              

                                echo "<div class=input-group style=width:25%>
                                    <span class=input-group-addon>";
                                $data = array('name'=>'cbox0','type'=>'checkbox');
                                echo form_input($data)."</span>";
                                $data = array('name'=>'input_cbox0','type'=>'text', 'class'=>'form-control','value'=>'Kurang Dokumen Syarat Bayar','readonly'=>TRUE);
                                echo form_input($data)."</div><br>";

                                echo "<div class=input-group style=width:25%>
                                    <span class=input-group-addon>";
                                $data = array('name'=>'cbox1','type'=>'checkbox');
                                echo form_input($data)."</span>";
                                $data = array('name'=>'input_cbox1','type'=>'text', 'class'=>'form-control','value'=>'Salah Dokumen Syarat Bayar','readonly'=>TRUE);
                                echo form_input($data)."</div><br>";

                                echo "<div class=input-group style=width:25%>
                                    <span class=input-group-addon>";
                                $data = array('name'=>'cbox2','type'=>'checkbox');
                                echo form_input($data)."</span>";
                                $data = array('name'=>'input_cbox2','type'=>'text', 'class'=>'form-control','value'=>'Kurang Data Surat Jalan/Tanda Terima','readonly'=>TRUE);
                                echo form_input($data)."</div><br>";

                                echo "<div class=input-group style=width:25%>
                                    <span class=input-group-addon>";
                                $data = array('name'=>'cbox3','type'=>'checkbox');
                                echo form_input($data)."</span>";
                                $data = array('name'=>'input_cbox3','type'=>'text', 'class'=>'form-control','placeholder'=>'Lainnya...'); 
                                echo form_input($data)."</div><br>";
                              
                                $data = array('type' => 'submit','class'=> 'btn btn-warning','name'=> 'proceed','value' => 'Save & Reject');
                                echo form_submit($data);      
                          }
                    }
                    else{ 
                      if(isset($no_po) && !(isset($nama_vendor)))
                        echo "<br><br>Nomor PO <b>".$no_po."</b> Tidak Ditemukan";
                      
                      
                    }  
                  }
                  else{
                    echo "<div class='alert alert-danger alert-dismissible'>
                            <button type=button class=close data-dismiss=alert aria-hidden=true>&times;</button>
                            <h4><i class='icon fa fa-ban'></i> Alert!</h4>
                            Anda tidak memiliki otoritas untuk mengakses halaman ini. Hubungi Administrator untuk mengajukan permohonan akses halaman.
                            <br>You do not authorized to access this page. Please contact our Administrator to get access to this page.
                          </div>";
                  }
                  echo "</div><!-- /.box-body --></div>";
                }
                else{
                }
              ?>
                
          </div>
          <!-- /.box -->
        </div>
       </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

</body>
</html>

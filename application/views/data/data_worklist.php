<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<head>
  <style>
    .processing{
      background-color:#2de053;
    }
    .rejected{
     background-color:#ff5e5e; 
    }
    .returned{
     background-color:#f6ff02; 
    }
    .normal{

    }
  </style>
</head>


                <!-- TABEL DATA DENDA -->
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                  
                <thead>
                <tr style="font-size:10px;">
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>No. Invc.</th>
                  <th>Status Invc.</th>
                  <th>Tanggal Invc.</th>
                  <th>Alasan Reject</th>
                  <!-- PIC Loket -->
                  <th>PIC</th>
                  <th>Tgl. Awal</th>
                  <th>Tgl. Akhir</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $no=0;
                   if(isset($data_worklist)){


                    foreach ($data_worklist as $wl){
                      //POST LIST
                      $post_list = array("loket","gr","unblocktermin","reviewdenda","pembayaran");
                      
                      // DATE
                      foreach($post_list as $check_date){
                        $cd = "status_".$check_date."_date_out";
                        if($wl[$cd]=="0000-00-00"){
                          $dd = "DD_".$check_date;
                          $sd = "SLA_".$check_date;
                          if($wl[$dd]==NULL && $wl[$sd]!=NULL)
                            $wl[$cd]=$wl["sd"]." day(s)";
                          else
                            $wl[$cd]=NULL;
                        }   
                      }


                      // CEK STATUS
                      foreach($post_list as $check_status){
                        $sd = "status_".$check_status;
                        $cd = "class_".$check_status;
                          
                        if($wl[$sd]=="Accepted")
                          $$cd = "processing";
                        else if($wl[$sd]=="Returned")
                          $$cd = "returned";
                        else if($wl[$sd]=="Rejected")
                          $$cd = "rejected";
                        else if($wl[$sd]=="Processing")
                          $$cd = "processing";
                        //else if($wl[$sd]=="Done")
                          //$cd = "done";
                        else
                          $$cd = "normal";
                      }


                      if(!isset($wl["status_loket_reason"]))
                        $$wl["status_loket_reason"]="-";

                      $no++;
                      echo "
                          <tr>
                            <td style=''>".$no."</td>
                            <td>".$wl["purchasing_document"]."</td>

                            <!-- INVOICE -->
                            <td>".$wl["invoice_number"]."</td>
                            <td align=center>".$wl["status_loket"]."</td>
                            <td align=center>".$wl["status_loket_date_in"]."</td>";

                        echo "
                            <td>".$wl["status_loket_reason"]."</td>";
                        
                        //PIC Loket
                        if($wl["pic_loket"]==$userdata["user_id"])
                          echo "
                            <td class=".$class_loket.">".$wl["pic_loket"]."</td>
                            <td class=".$class_loket.">".$wl["status_loket_date_in"]."</td>
                            <td class=".$class_loket.">".$wl["status_loket_date_out"]."</td>";

                        //PIC GR
                        if($wl["pic_gr"]==$userdata["user_id"])
                        echo "
                            <td>".$wl["pic_gr"]."</td>
                            <td>".$wl["status_gr_date_in"]."</td>
                            <td>".$wl["status_gr_date_out"]."</td>";

                        //PIC Unblock Termin
                        if($wl["pic_unblocktermin"]==$userdata["user_id"])
                        echo "
                            <td>".$wl["pic_unblocktermin"]."</td>
                            <td>".$wl["status_unblocktermin_date_in"]."</td>
                            <td>".$wl["status_unblocktermin_date_out"]."</td>";

                        //PIC Review Denda
                        if($wl["pic_reviewdenda"]==$userdata["user_id"])
                        echo "
                            <td>".$wl["pic_reviewdenda"]."</td>
                            <td>".$wl["status_reviewdenda_date_in"]."</td>
                            <td>".$wl["status_reviewdenda_date_out"]."</td>";

                        //PIC Pembayaran
                        if($wl["pic_pembayaran"]==$userdata["user_id"])
                        echo "
                            <td>".$wl["pic_pembayaran"]."</td>
                            <td>".$wl["status_pembayaran_date_in"]."</td>
                            <td>".$wl["status_pembayaran_date_out"]."</td>
                          ";
                        $id = $wl['purchasing_document'];
                        $id2 = $wl['invoice_number'];
                        //Action Button
                        //echo "<td><a href=".site_url()."/myworklist/edit/$ss><div class='btn-sm btn-info btn-dropbox'><center>View</div></a>".$this->postCURL($url, $params)."</td>";
                          

                        ?>
                          <td><a href="<?=site_url('myworklist/edit/'.$id.'/'.$id2); ?>" class="btn btn-success">View</a></td>
                        <?php

                        /*
                        echo form_open('myworklist/edit');

                        $data = array('name'=>'no_po_edit','class'=>'btn-sm btn-info btn-dropbox','type'=>'text','value' => $ss,'style'=>'display:none');
                        echo form_input($data);

                        $data = array('name'=>'submitbutton','class'=>'btn-sm btn-info btn-dropbox','type'=>'submit','value' => $ss);
                        echo "<td>".form_input($data)."</td>";

                        echo form_close();
                        */
                        echo "</tr>";
                        
                    }
                  }
                
                ?>

                </tbody>
                <tfoot>
                </tfoot>
              </table>
              <!-- AKHIR TABEL DATA DENDA -->
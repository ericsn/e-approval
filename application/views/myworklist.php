<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    
    <?php  ?>

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="<?=site_url('tagihan')?>">Tagihan</a></li>
        <li><a href="<?=site_url('tanda_terima')?>">Tanda Terima</a></li>
        <li class="active"><a href="<?=site_url('myworklist')?>">Worklist</a></li>
      </ol>
    </section>
    <br>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      



      <div class="row">
        <div class="col-md-12">
          

                <?php
                  if(isset($no_po_edit) && $no_po_edit!=""){
                    echo "<div class=box-body style=width:auto><div class=
                          box>";
                    if($userdata["user_auth"]=="counter")
                      $this->load->view('edit/loket/index.php');
                    echo "</div></div>";
                  }
                ?>

      <div class="box">
            <!-- START LINE-->
            <div class="box-body">
              <?php
                if($userdata["auth_2"]==1){
                  echo "<h3>My Worklist</h3>";
                  $this->load->view("data/data_worklist");

                        //Action Button
                        echo form_open('myworklist/edit');

                        $data = array('name'=>'no_po_edit','class'=>'form-control','type'=>'text','value' => "asa", 'readonly'=>TRUE, 'style'=>'display:none');
                        echo form_input($data);

                        $data = array('name'=>'submit','class'=>'btn-sm btn-info btn-dropbox','type'=>'submit','value' => "View");
                        echo "<td>".form_input($data)."</td>";

                        echo form_close();
                }
                else{
                  $this->load->view("errors/no_authority");
                }
              ?>
            </div>
            <!-- END LINE | CALL SEARCH BAR -->

            
        </div>
       </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

</body>
</html>

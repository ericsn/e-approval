<head>
  <title>E-Approval BCA</title>
  <link rel="icon" href="<?=base_url()?>assets/img/icon_bca.png">
</head>

<header class="main-header">

    <!-- Logo -->
    <a href="<?=site_url('')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini" style="font-size:10px"><b>E-</b>Approval</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>E-</b>Approval &nbsp<img src="<?=base_url()?>assets/img/logo_bca.png" style="width:70px"> </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new task(s)
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> 5 task(s) <b>Changed</b> by system
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 3 task(s) <b>returned</b>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="<?=site_url('myworklist')?>">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><?php echo $userdata["user_name"]; ?> </span>
              &nbsp&nbsp<span><i class="fa fa-user"></i></span>
            </a>

            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url()?>assets/img/user_img/<?=$userdata["user_id"]?>.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo strtoupper($userdata["user_auth"]); ?>
                  <small>User ID : <?=$userdata["user_id"]?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Personal Info</a>
                </div>
                <div class="pull-right">
                  <a href="<?= site_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>


  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <?php
        if($default_pos=="tagihan"){
          $ul_1="active treeview menu-open";
          $ul_2="treeview menu-close";
            if($default_pos2=="terima_tagihan")
              {$li_1_1="active";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";
                $li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";}
            if($default_pos2=="cek_tagihan")
              {$li_1_1="nonactive";$li_1_2="active";$li_1_3="nonactive";$li_1_4="nonactive";
                $li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";}
            if($default_pos2=="tanda_terima")
              {$li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="active";$li_1_4="nonactive";
                $li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";}
            if($default_pos2=="report")
              {$li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="active";
                $li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";}
        }
        else{
          $ul_1="treeview menu-close";
          $ul_2="active treeview menu-open";
            if($default_pos2=="terima_dokumen")
              {$li_2_1="active";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";}
            if($default_pos2=="tanda_terima")
              {$li_2_1="nonactive";$li_2_2="active";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";}
            if($default_pos2=="prakualifikasi")
              {$li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="active";$li_2_4="nonactive";$li_2_5="nonactive";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";}
            if($default_pos2=="monitoring")
              {$li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="active";$li_2_5="nonactive";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";}
            if($default_pos2=="report")
              {$li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="active";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";}

            if($default_pos2=="dashboard"){
                $ul_2="treeview menu-close";
                $li_2_1="nonactive";$li_2_2="nonactive";$li_2_3="nonactive";$li_2_4="nonactive";$li_2_5="nonactive";
                $li_1_1="nonactive";$li_1_2="nonactive";$li_1_3="nonactive";$li_1_4="nonactive";              
            }
          }
      ?>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGATION</li>
        <li><a href="<?= site_url('dashboard')?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class="<?php echo $ul_1?>">

          <a href="#">
            <i class="fa fa-money"></i> <span>Invoice</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if($userdata["auth_2"]=='1'){ ?>
              <li class="<?php echo $li_1_1?>"><a href="<?= site_url('terima_tagihan')?>"><i class="fa fa-circle-o"></i> Terima Tagihan</a></li>
            <?php } ?>
            <?php if($userdata["auth_1"]=='1'){ ?>
              <li class="<?php echo $li_1_2?>"><a href="<?= site_url('cek_tagihan')?>"><i class="fa fa-circle-o"></i> Cek Tagihan</a></li>
            <?php } ?>
            <?php if($userdata["auth_5"]=='1'){ ?>
              <li class="<?php echo $li_1_3?>"><a href="<?= site_url('tanda_terima')?>"><i class="fa fa-circle-o"></i> Tanda Terima</a></li>
            <?php } ?>
            
              <li class="<?php echo $li_1_4?>"><a href="<?= site_url('report_tagihan')?>"><i class="fa fa-circle-o"></i> Report</a></li>
            
          </ul>
        </li>
        <li class="<?php echo $ul_2?>">
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Dokumen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $li_2_1?>"><a href="index.html"><i class="fa fa-circle-o"></i> Penerimaan Dokumen
            </a></li>
            <li class="<?php echo $li_2_2?>"><a href="index2.html"><i class="fa fa-circle-o"></i> Tanda Terima
            </a></li>
            <li class="<?php echo $li_2_3?>"><a href="index.html"><i class="fa fa-circle-o"></i> Prakualifikasi</a></li>
            <li class="<?php echo $li_2_4?>"><a href="index2.html"><i class="fa fa-circle-o"></i> Monitoring</a></li>
            <li class="<?php echo $li_2_5?>"><a href="index2.html"><i class="fa fa-circle-o"></i> Report</a></li>
          </ul>
        </li>
        
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>

        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="<?=site_url('dashboard')?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Cek Tagihan</li>
      </ol>
    </section>
    <br>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      



      <div class="row">
              <div class="col-md-12">
             
                <div class="box">
            <div class="box-header">
              <h3 class="box-title">Input Data Tagihan</h3>

            </div>

            <div>
                        

                <?php
                  echo form_open('cek_tagihan/search_result', 'class="sidebar-form"');
                  ?>
                  <div class="input-group">
                  <?php
                  $data = array('name'=>'no_po','placeholder'=>'Nomor PO/Kontrak','class'=>'form-control','id'=>'cobaaja1');
                  echo form_input($data);
                ?>
                          <span class="input-group-btn">
                            <?php
                              $data = array(
                              'type' => 'submit',
                              'id'=> 'search-btn',
                              'class'=> 'btn btn-flat',
                              'name'=> 'search'
                              );
                              echo form_submit($data); 
                            ?>
                            
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                <div class="btn btn-success" data-toggle="modal" data-target="#modal-success">Recieve</div>
                <div class="btn btn-danger">Reject</div>

                <div class="modal modal-success fade" id="modal-success">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Terima Tagihan</h4>
                      </div>
                      <div class="modal-body">
                        <p>One fine body&hellip;</p>  
                        <input type="text" id="cobaaja2">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                        <button onclick="autoFill()" type="button" class="btn btn-outline">Simpan</button>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php $number_uncompleted_tasks = 0; ?>

                <?php //$call_termin;?>
                <?php
                  if(isset($data_turunan)){ 
                    $this->load->view("data_termin");
                    $this->load->view("data_inb_delivery");
                    $this->load->view("data_denda");
                  }
                ?>

                <?php 
                  //if($number_uncompleted_tasks>0)
                    //echo "<a><div class='btn btn-info'>Send To</div></a>";
                ?>

                <br><br>


            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->

              </div>
       </div>


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

</body>
</html>

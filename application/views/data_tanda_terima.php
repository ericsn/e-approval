<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<head>
  <style>
    .processing{
      background-color:#2de053;
    }
    .rejected{
     background-color:#ff5e5e; 
    }
    .returned{
     background-color:#f6ff02; 
    }
    .normal{

    }
  </style>
</head>


                <!-- TABEL DATA DENDA -->
                <table id="example1" class="table table-bordered table-striped" style="font-size:12px;">
                  
                <thead>
                <tr style="font-size:10px;">
                  <th rowspan="2">No</th>
                  <th rowspan="2">Purch. Doc.</th>
                  <!-- Invoice -->
                  <th rowspan="2">Invoice</th>
                  <th rowspan="2">Alasan</th>
                  <th colspan="2">Loket</th>
                  <th colspan="2">GR</th>
                  <th colspan="2">Unblock Termin</th>
                  <th colspan="2">Review Denda</th>
                  <th colspan="2">Pembayaran</th>
                  <th rowspan="2">Action</th>
                </tr>
                <tr style="font-size:10px;">
                  <!-- PIC Loket -->
                  <th>PIC</th>
                  <th>Tgl. Akhir</th>
                  <!-- PIC GR -->
                  <th>PIC</th>
                  <th>Tgl. Akhir</th>
                  <!-- PIC Unblock Termin -->
                  <th>PIC</th>
                  <th>Tgl. Akhir</th>
                  <!-- PIC Review Denda -->
                  <th>PIC</th>
                  <th>Tgl. Akhir</th>
                  <!-- PIC Pembayaran -->
                  <th>PIC</th>
                  <th>Tgl. Akhir</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $no=0;
                   if(isset($data_tanda_terima)){


                    foreach ($data_tanda_terima as $tanda_terima){
                      //POST LIST
                      $post_list = array("loket","gr","unblocktermin","reviewdenda","pembayaran");
                      
                      // DATE
                      foreach($post_list as $check_date){
                        $cd = "status_".$check_date."_date_out";
                        if($tanda_terima[$cd]=="0000-00-00"){
                          $dd = "DD_".$check_date;
                          $sd = "SLA_".$check_date;
                          if($tanda_terima[$dd]==NULL && $tanda_terima[$sd]!=NULL)
                            $tanda_terima[$cd]=$tanda_terima["sd"]." day(s)";
                          else
                            $tanda_terima[$cd]=NULL;
                        }   
                      }


                      // CEK STATUS
                      foreach($post_list as $check_status){
                        $sd = "status_".$check_status;
                        $cd = "class_".$check_status;
                          
                        if($tanda_terima[$sd]=="Accepted")
                          $$cd = "accepted";
                        else if($tanda_terima[$sd]=="Returned")
                          $$cd = "returned";
                        else if($tanda_terima[$sd]=="Rejected")
                          $$cd = "rejected";
                        else if($tanda_terima[$sd]=="Processing")
                          $$cd = "processing";
                        //else if($tanda_terima[$sd]=="Done")
                          //$cd = "done";
                        else
                          $$cd = "normal";
                      }


                      $no++;
                      echo "
                          <tr>
                            <td style=''>".$no."</td>
                            <td>".$tanda_terima["purchasing_document"]."</td>

                            <!-- INVOICE -->
                            <td>".$tanda_terima["invoice_number"]."</td>";

                        if($tanda_terima["status_loket_reason"]=="")
                          $tanda_terima["status_loket_reason"]="-";
                        echo "
                            <td>".$tanda_terima["status_loket_reason"]."</td>";
                        
                        //PIC Loket
                        
                        echo "
                          <td class=".$class_loket.">".$tanda_terima["pic_loket"]."</td>
                          <td class=".$class_loket.">".$tanda_terima["status_loket_date_out"]."</td>";

                        //PIC GR
                        echo "
                            <td class=".$class_gr.">".$tanda_terima["pic_gr"]."</td>
                            <td class=".$class_gr.">".$tanda_terima["status_gr_date_out"]."</td>";

                        //PIC Unblock Termin
                        echo "
                            <td class=".$class_unblocktermin.">".$tanda_terima["pic_unblocktermin"]."</td>
                            <td class=".$class_unblocktermin.">".$tanda_terima["status_unblocktermin_date_out"]."</td>";

                        //PIC Review Denda
                        echo "
                            <td class=".$class_reviewdenda.">".$tanda_terima["pic_reviewdenda"]."</td>
                            <td class=".$class_reviewdenda.">".$tanda_terima["status_reviewdenda_date_out"]."</td>";

                        //PIC Pembayaran
                        echo "
                            <td class=".$class_pembayaran.">".$tanda_terima["pic_pembayaran"]."</td>
                            <td class=".$class_pembayaran.">".$tanda_terima["status_pembayaran_date_out"]."</td>
                          ";

                        //Action Button
                        echo form_open('myworklist/edit');

                        $data = array('name'=>'no_po','class'=>'form-control','type'=>'text','value' => $tanda_terima["purchasing_document"], 'readonly'=>TRUE, 'style'=>'display:none');
                        echo form_input($data);

                        $data = array('name'=>'submit','class'=>'btn-sm btn-info btn-dropbox','type'=>'submit','value' => "Edit");
                        echo "<td>".form_input($data)."</td>";

                        echo form_close();
                        
                    }
                  }
                
                ?>

                </tbody>
                <tfoot>
                </tfoot>
              </table>
              <!-- AKHIR TABEL DATA DENDA -->
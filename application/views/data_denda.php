<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

                <!-- TABEL DATA DENDA -->
                <table id="example2" class="table table-bordered table-striped" style="font-size:12px;">
                  <h4><b>DENDA</b></h4>
                <thead>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Inb. Delivery</th>
                  <th>Plant</th>
                  <th>Stor. Loc.</th>
                  <th>Adjustmend Amt</th>
                  <th>Currency</th>
                  <th>Sts Denda</th>
                  <th>Created on</th>
                  <th>Changed on</th>
                  <th>Changed by</th>
                  <th>Alasan</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $number_uncompleted_tasks=0;
                  $no=0;
                   if(isset($data_denda)){
                    foreach ($data_denda as $denda){
                      $no++;
                      if($denda["status_denda"]=="Blocked" || $denda["status_denda"]=="Created")
                        echo "<tr style='padding-left:100px;'>";
                      else{
                        echo "<tr style='padding-left:100px;background-color:#e96666;color:white;'>";
                        $number_uncompleted_tasks++;
                      }
                       
                      echo "
                          <td>".$no."</td>
                          <td>".$denda["purchasing_document"]."</td>
                          <td>".$denda["inbound_delivery"]."</td>
                          <td>".$denda["plant"]."</td>
                          <td>".$denda["storage_location"]."</td>
                          <td>".$denda["adjustment_amount"]."</td>
                          <td>".$denda["currency"]."</td>
                          <td>".$denda["status_denda"]."</td>
                          <td>".$denda["created_on"]."</td>
                          <td>".$denda["changed_on"]."</td>
                          <td>".$denda["changed_by"]."</td>
                          <td>".$denda["alasan"]."</td>
                          </tr>
                          ";
                    }
                  }
                
                ?>

                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Purch. Doc.</th>
                  <th>Inb. Delivery</th>
                  <th>Plant</th>
                  <th>Stor. Loc.</th>
                  <th>Adjustmend Amt</th>
                  <th>Currency</th>
                  <th>Sts Denda</th>
                  <th>Created on</th>
                  <th>Changed on</th>
                  <th>Changed by</th>
                  <th>Alasan</th>
                </tr>
                </tfoot>
              </table>
              <!-- AKHIR TABEL DATA DENDA -->
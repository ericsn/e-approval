<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="<?=site_url('dashboard')?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      </ol>
    </section>
    <br>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      



      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header bg-aqua">
              <h3 class="box-title">Invoice</h3>
            </div>
            <div class="box-body">
              <centre><table><tr>
              <?php
                if($userdata['auth_3']==1)
                  echo "<td><a href=".site_url('worklist').">
                     <div style='margin-right:40px '>
                        <span class='info-box-icon bg-aqua' style='width:150px;height:150px'>
                          <i style='margin-top:25px;font-size:90px' class='fa fa-television'></i>
                        </span>
                        <br>
                        <p align='center'>
                          <b>Worklist</b>
                        </p>                      
                      </div>
                  </a><td></td>";

                if($userdata['auth_2']==1)
                  echo "<td><a href=".site_url('terima_tagihan').">
                     <div style='margin-right:40px '>
                        <span class='info-box-icon bg-aqua' style='width:150px;height:150px'>
                          <i style='margin-top:25px;font-size:90px' class='fa fa-get-pocket'></i>
                        </span>
                        <br>
                        <p align='center'>
                          <b>Terima Tagihan</b>
                        </p>                      
                      </div>
                  </a><td></td>";

                if($userdata['auth_1']==1)
                  echo "<td><a href=".site_url('cek_tagihan').">
                     <div style='margin-right:40px '>
                        <span class='info-box-icon bg-aqua' style='width:150px;height:150px'>
                          <i style='margin-top:25px;font-size:90px' class='fa fa-newspaper-o'></i>
                        </span>
                        <br>
                        <p align='center'>
                          <b>Cek Tagihan</b>
                        </p>                      
                      </div>
                  </a><td></td>";

                  
              ?>
            </tr></table>
            </div>
            <br>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>



      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header bg-yellow">
              <h3 class="box-title">Monitor</h3>
            </div>
            <div class="box-body">
              <centre><table><tr>
              <?php

                if($userdata['auth_5']==1)
                  echo "<td><a href=".site_url('tanda_terima').">
                     <div style='margin-right:40px '>
                        <span class='info-box-icon bg-yellow' style='width:150px;height:150px'>
                          <i style='margin-top:25px;font-size:90px' class='fa fa-flag-checkered'></i>
                        </span>
                        <br>
                        <p align='center'>
                          <b>Tanda Terima</b>
                        </p>                      
                      </div>
                  </a><td></td>";
                
                echo "<td><a href=".site_url('report_tagihan').">
                     <div style='margin-right:40px '>
                        <span class='info-box-icon bg-yellow' style='width:150px;height:150px'>
                          <i style='margin-top:25px;font-size:90px' class='fa fa-bar-chart'></i>
                        </span>
                        <br>
                        <p align='center'>
                          <b>Report</b>
                        </p>                      
                      </div>
                  </a><td></td>";
              ?>
            </tr></table>
            </div>
            <br>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>



     </div>


      
      

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

</body>
</html>

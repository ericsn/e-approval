<head>
	<style>
		.form-control{
			margin-bottom:3%;
		}
	</style>
</head>
<?php
	if($no_po_edit!=""){
?>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><b>PENERIMAAN INVOICE VENDOR</b> | NOMOR PO : <?=$no_po_edit
			      ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
      
    	    <div class="box-header with-border">
			      <h3 class="box-title"><b>Nomor Invoice</b>
	                  <select id="form_inv" name="form_inv" onchange="inv_change(this.value)">
	                  <?php
	                  	foreach($data_worklist as $opt){
	                  		//echo "<a href=".site_url('worklist/edit/'.$opt["purchasing_document"].'/'.$opt["invoice_number"]).">";
                  		  if($opt["purchasing_document"]==$no_po_edit){
                  		  	if(isset($opt["invoice_number"])){
		                  		echo "<option value=".$opt["invoice_number"];
		                  		if($opt["invoice_number"]==$invoice_number)
		                  			echo " selected>";
		                  		else
		                  			echo ">";
		                  		echo $opt["invoice_number"]."</option>";
		                  	}
		                  	else{
		                  		echo"A";
		                  	}
                  	      }
	                  	}
	                  ?>
	                  </select>
                	</h3>
			    </div>
			    <!-- /.box-header -->
			    <!-- form start -->

			    <?php
			    	foreach($data_worklist as $wl2)
			    		if($wl2["purchasing_document"]==$no_po_edit && $wl2["invoice_number"]==$invoice_number)
			    			$data_now = $wl2;
			    ?>

			    <form role="form" >
			      <div class="box-body">
			      	<table>

			      		<tr>
				          <td><t>
					          Nomor Invoice
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" name="invoice_number"	
			      		<?php
			      		if($data_invoice["invoice_number"]!=""){
			      			echo "value='".$data_invoice['invoice_number']."' readonly>";
			      		}
			      		else
			      			echo ">";
			      		?>
			      			</td>
			         	</tr>
			      		<tr>
				          <td style="margin-bottom:100px">
				          	Tanggal Invoice
				          </td>
				          <td>
				          	<div class="input-group date" >
			                  <div class="input-group-addon" >
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input type="text" class="form-control pull-right" id="datepicker" name="tanggal_invoice">
			                </div>
				      		</td>
				         </tr>
			      		<tr>
				          <td>
					          No. Antrian
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" value="22" name="no_antrian" readonly></td>
				         </tr>
			      		<tr>
				          <td>
					          Tanggal Terima
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" value="<?=date('Y/m/d')?>" name="tanggal_terima" readonly>
					      </td>
				         </tr>
			      		<tr>
				          <td><t>
					          Nomor PO/Kontrak&nbsp&nbsp&nbsp
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" value="<?=$data_invoice['purchasing_document']?>" name="purchasing_document" readonly>	
					      </td>
				         </tr>
			      		<tr>
				          <td>
					          Inbound Delivery
					      </td>
					      <td>
					          <textarea id="temp_id" class="form-control" id="exampleInputEmail1" value=""  placeholder="Nomor ID" readonly data-toggle="modal" data-target="#modal-success" name="id"><?=$data_now['id']?></textarea>

					         </td>
					         <td>
					          <a class="btn btn-info" data-toggle="modal" data-target="#modal-success">...</a>
					      </td>
				         </tr>
			      		<tr>
				          <td>
					          Total Invoice
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="10000000"  name="total_invoice">
					      </td>
				         </tr>
			      		<tr>
				          <td>
					          Mata Uang
					      </td>
					      <td>
					          <select class="form-control" name="currency">
					          	<option>IDR</option>
					          	<option>HKG</option>
					          	<option>USD</option>
					          	<option>SGD</option>
					          	<option>EUR</option>
					          </select>
					      </td>
				         </tr>
			      		<tr>
				          <td>
					          Jenis Tagihan
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tagihan"  name="jenis_tagihan">
					      </td>
				         </tr>
			      		<tr>
				          <td>
					          Untuk Tagihan
					      </td>
					      <td>
					          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Untuk Beli Komputer" name="untuk_tagihan">
					      </td>
					  </tr>
					</table>



				    <!-- MODAL-->
				    <div class="modal modal-info fade" id="modal-success">
				    	<br><br><br><br><br><br><br><br>
				    	<br><br><br><br>
	                  <div class="modal-dialog" style="width:25%;">
	                    <div class="modal-content">
	                      <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                          <span aria-hidden="true">&times;</span></button>
	                        <h4 class="modal-title">Masukkan Inbound Delivery</h4>
	                      </div>
	                      <!-- modal body -->
	                      <center>
	                      <div id="temp_inv" style="font-size:16px">
	                      	
	                        <select type="text" id="myRangex1" class="range1">
	                        	<?php
	                        		foreach($id_po as $idpo)
	                        			echo "<option>".$idpo["inbound_delivery"]."</option>";
	                        	?>
	                        </select>
	                        <i class="fa fa-arrow-circle-right"></i>
	                        <select type="text" id="myRangey1" class="range2">
	                        	<?php
	                        		foreach($id_po as $idpo)
	                        			echo "<option>".$idpo["inbound_delivery"]."</option>";
	                        	?>
	                        </select>

	                      </div>
	                      <!-- END OF modal body -->
	                      
	                      <div class="modal-footer">
	                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Tutup</button>
	                        <button onclick="addmyFunction()" type="button" class="btn btn-outline">Tambah</button>
	                        <button onclick="myFunction()" type="button" class="btn btn-outline">Simpan</button>
	                      </div>
	                    </div>
	                    <!-- /.modal-content -->
	                  </div>
	                  <!-- /.modal-dialog -->
	                </div>
	                <!-- /.modal -->

			      </div>
			      <!-- /.box-body -->

			      <div class="box-footer">
			        <button type="submit" class="btn btn-primary">Submit</button>
			      </div>
			    </form>


    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

<?php
}
?>

<script>
	// FUNCTION TO DIRECTLY CHANGE PAGE OF CHANGING INVOICE NUMBER
	function inv_change(val){
		var urln = "<?php site_url('/worklist/edit/$no_po_edit/')?>"+val;
		location.replace(urln);
	}
</script>

<script>
function myFunction() {
	//CLEAR THE INBOUND DELIVERY TEXTAREA
	document.getElementById("temp_id").value="";
	//

	// READ EXISTING SELECT OPTION NUMBER
  	var num = document.querySelectorAll('.range1').length+1;
  	//

  	// START SEND VALUE OF INBOUND DELIVERY
  	for(i=1;i<=num;i++){
  		// GET INBOUND DELIVERY RANGE as X and Y
	    var x = document.getElementById("myRangex"+i).value;
	    var y = document.getElementById("myRangey"+i).value;
	    //

	    // KEEP THE HIGHER NUMBER INBOUND DELIVERY as X
	    if(x>y){
	    	z=x;
	    	x=y;
	    	y=z;
	    }
	    //

	    // WRITE FOR RANGE ID
	    if(y!=x){
	      document.getElementById("temp_id").value += x+"-"+y+';';
	    }
	    // WRITE FOR SINGLE ID
	    else
	      if(x>0)
	          document.getElementById("temp_id").value += x+";";
 	}
 	// END SEND VALUE OF INBOUND DELIVERY
}
</script>

<script>
function addmyFunction() {
	// READ EXISTING SELECT OPTION, AND DECLARE NEW SELECT OPTION NUMBER
	var num = document.querySelectorAll('.range1').length +1;
	//

	var mybr = document.createElement('br');
	document.getElementById("temp_inv").appendChild(mybr);
	
	// DECLARE THE NEW SELECT AND OPTION BUTTON
	var selectx = document.createElement("select");
	selectx.setAttribute("id", "myRangex"+num);
 	selectx.setAttribute("class", "range1");
 	var selecty = document.createElement("select");
 	selecty.setAttribute("id", "myRangey"+num);
 	selecty.setAttribute("class", "range2");
 	//

 	// START ADDING INBOUND DELIVERY LIST
 	<?php
	foreach($id_po as $idpo){
	?>
		var optionx = document.createElement("option");
		var val = <?=$idpo["inb_delivery"]?>;
		optionx.setAttribute("value",val);
		optionx.innerHTML = <?php echo $idpo["inb_delivery"]?>;
		selectx.appendChild(optionx);
		var optiony = document.createElement("option");
		var val = <?=$idpo["inb_delivery"]?>;
		optiony.setAttribute("value",val);
		optiony.innerHTML = <?php echo $idpo["inb_delivery"]?>;
		selecty.appendChild(optiony);
	<?php
	}
	?>
	// END OF ADDING INBOUND DELIVERY LIST
	
	// CENTER THE DISPLAY
	var mycntr = document.createElement('center');
	document.getElementById("temp_inv").appendChild(mycntr);
	//

	// DISPLAY THE SELECT X
	document.getElementById("temp_inv").appendChild(selectx);
	//

	// DISPLAY THE ARROW ICON
	var myarr = document.createElement('i');
	myarr.setAttribute("class", "fa fa-arrow-circle-right");
	document.getElementById("temp_inv").appendChild(myarr);
	//

	// DISPLAY THE SELECT Y
	document.getElementById("temp_inv").appendChild(selecty);
	//
 }
</script>
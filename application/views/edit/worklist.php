<?php
	if($no_po_edit!=""){
?>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Nomor PO/Kontrak</b> : <?=$no_po_edit
			      ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
      
    	    <div class="box-header with-border">
			      <h3 class="box-title"><b>Nomor Invoice</b>
	                  <select id="form_inv" name="form_inv" onchange="inv_change(this.value)">
	                  <?php
	                  	foreach($data_worklist as $opt){
	                  		//echo "<a href=".site_url('worklist/edit/'.$opt["purchasing_document"].'/'.$opt["invoice_number"]).">";
	                  		echo "<option value=".$opt["invoice_number"];
	                  		if($opt["invoice_number"]==$invoice_number && $opt["purchasing_document"]==$no_po_edit)
	                  			echo " selected>";
	                  		else
	                  			echo ">";
	                  		echo $opt["invoice_number"]."</option>";
	                  	}
	                  ?>
	                  </select>
                	</h3>
			    </div>
			    <!-- /.box-header -->
			    <!-- form start -->
			    <form role="form">
			      <div class="box-body">
			        <div class="form-group">
			          <label for="exampleInputEmail1">Nomor PO/Kontrak</label>
			          <input type="email" class="form-control" id="exampleInputEmail1" value="<?=$no_po_edit?>" readonly>
			        </div>
			        <div class="form-group">
			          <label for="exampleInputPassword1">Password</label>
			          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			        </div>
			        <div class="form-group">
			          <label for="exampleInputFile">File input</label>
			          <input type="file" id="exampleInputFile">

			          <p class="help-block">Example block-level help text here.</p>
			        </div>
			      </div>
			      <!-- /.box-body -->

			      <div class="box-footer">
			        <button type="submit" class="btn btn-primary">Submit</button>
			      </div>
			    </form>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

<?php
}
?>

<script>
	function inv_change(val){
		var urln = "<?php site_url('/worklist/edit/4200004822/')?>"+val;
		location.replace(urln);
	}
	</script>
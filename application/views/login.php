<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/adminlte/plugins/iCheck/square/blue.css">


</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Counter </b>View</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

   <?php
        echo form_open('login/process');
          echo "<div class='form-group has-feedback'>";
            $data = array('name'=>'user_id','placeholder'=>'User ID','class'=>'form-control','required'=>true);
            echo form_input($data);
            echo "<span class='fa fa-user-md form-control-feedback'></span>
            </div>";
          echo "<div class='form-group has-feedback'>";
            $data = array('name'=>'password', 'type'=>'password','placeholder'=>'Password','class'=>'form-control','required'=>true);
            echo form_input($data);
            echo "<span class='glyphicon glyphicon-lock form-control-feedback'></span>
            </div>";

          echo "<div class=row>
                  <div class=col-xs-4>";
          $data = array('type' => 'submit','class'=> 'btn btn-primary btn-block btn-flat','name'=> 'submit','value' => 'Masuk');
          echo form_submit($data)."</div></div>"; 
    ?>
      <br>
      <?php
        if(isset($error_message))
          echo "<p style='color:red' class=login-box-msg>".$error_message."</p>";
      ?>

    <div class="social-auth-links text-center">
      <p>----------------- or -----------------</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-send"></i> PERMOHONAN PENAMBAHAN USER</a>
      <a href="#" class="btn btn-block btn-social btn-primary btn-flat"><i class="fa fa-refresh"></i> LUPA PASSWORD</a>
    </div>
    <!-- /.social-auth-links -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- iCheck -->
<script src="<?php echo base_url();?>adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>

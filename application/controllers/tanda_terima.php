<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tanda_terima extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model
         $this->load->model('m_tanda_terima');

        if($this->session->userdata('logged_in')!=TRUE)
        	redirect('login');


   		$this->load->view('default_css');
   		$pos=$this->deklarasi_pos();

        $pos["userdata"]=$this->session->userdata();
   		$this->load->view('default_header',$pos);
   	}

   	public function deklarasi_pos(){
   		$pos=array(
   			"default_pos"=>"tagihan",
   			"default_pos2"=>"tanda_terima"
   		);
   		return $pos;
   	}

   	public function index(){
         $data["data_tanda_terima"] = $this->m_tanda_terima->get_all_tanda_terima();

   		$this->load->view('tanda_terima', $data);

   	}


}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class myworklist extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model
   		$this->load->model('m_tanda_terima');	
      $this->load->model('m_general'); 


        if($this->session->userdata('logged_in')!=TRUE)
        	redirect('login');


   		$this->load->view('default_css');
   		$pos=$this->deklarasi_pos();

        $pos["userdata"]=$this->session->userdata();
   		$this->load->view('default_header',$pos);
   	}

   	public function deklarasi_pos(){
   		$pos=array( 
   			"default_pos"=>"tagihan",
   			"default_pos2"=>"tanda_terima"
   		);
   		return $pos;
   	}

    public function call_data(){
       $data["data_worklist"] = $this->m_tanda_terima->get_all_tanda_terima($this->session->userdata("user_id"));
       return $data;
    }

    public function get_tweets(){
      $this->load->library('Datatables');
       $this->datatables->select('user_id,user_name')->from('login_data');
       $this->datatables->add_column('edit', '<a class="btn btn-primary" href="profiles/edit/$1">EDIT</a>', 'user_id');
       $this->datatables->add_column('delete', '<a class="btn btn-danger" href="profiles/delete/$1">Delete</a>', 'user_id');
       echo $this->datatables->generate();
    }

   	public function index(){
      $data = $this->call_data();
   		$this->load->view('myworklist',$data);
   	} 



    public function edit($id="",$id2=""){
      $data = $this->call_data();
     // $data2 = $this->m_tanda_terima->get_all_worklist($this->session->userdata('user_id'));
      //$data["no_po_edit"] = $this->input->post('submitbutton');
      $data["no_po_edit"] = $id;
      $data["invoice_number"] = $id2;

      $data["id_po"]=$this->m_tanda_terima->get_all_id_po($id);

      $data["data_invoice"]=$this->m_tanda_terima->get_all_invoice_data($id,$id2);
      
      //if($data["no_po_edit"]!="")
      $this->load->view('myworklist',$data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model

        if($this->session->userdata('logged_in')!=TRUE)
        	redirect('login');

        $this->load->view('default_css');
        $pos=$this->deklarasi_pos();

        //SET userdata[] FROM session->userdata()
        $pos["userdata"]=$this->session->userdata();

        //load header & call $pos array to prepare $userdata[] and $defaultpos to be used in view:default_header
   		$this->load->view('default_header',$pos);
        
   	}

   	public function deklarasi_pos(){
   		$pos=array(
   			"default_pos"=>"",
   			"default_pos2"=>"dashboard"
   		);
   		return $pos;
   	}

	public function index(){
		$this->load->view('Dashboard');
		$this->load->model('m_data_po');
	}

	public function search_result(){
		//$data = array('no_po' => $this->input->post('no_po'));
		//$data = $this->get_termin_po($data['no_po']);
		$no_po = $this->input->post('no_po');

		$sql_inb_deliv = "SELECT * FROM data_inbound_delivery WHERE purchasing_document = $no_po";

		$sql_termin = "SELECT * FROM data_termin WHERE purchasing_document = $no_po";

		$sql_denda = "SELECT * FROM data_denda WHERE purchasing_document = $no_po";

		$data = array(
			"data_inb_deliv" => $this->db->query($sql_inb_deliv)->result_array(),
			"data_termin" => $this->db->query($sql_termin)->result_array(),
			"data_denda" => $this->db->query($sql_denda)->result_array(),
			"data_turunan" => $this
		);

		//$data['data_termin'] = $this->m_data_po->GetTermin();
		//$this->load->model('m_data_po');

		//$data2 = array("call_termin" => $this->load->view("data_inb_delivery", $data));
		$data["userdata"]=$pos;
        $this->load->view("Dashboard",$data);

	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model
        $this->load->view('default_css');
        $this->load->model('m_login');
   	}

	public function index(){
		$this->session->sess_destroy();
		$this->load->view('login');
	}

	public function process(){
		$user_id = $this->input->post('user_id');
		$password = $this->input->post('password');
		$test_login = $this->m_login->get_data_user($user_id,$password);
		if($test_login!=NULL){
			//echo $test_login["user_name"];

			//SET USER'S ROLE AUTH
			$login_auth = $this->m_login->get_user_role_auth($test_login['login_auth']);
			$this->session->set_userdata('auth_1',$login_auth['auth_1']);
			$this->session->set_userdata('auth_2',$login_auth['auth_2']);
			$this->session->set_userdata('auth_3',$login_auth['auth_3']);
			$this->session->set_userdata('auth_4',$login_auth['auth_4']);
			$this->session->set_userdata('auth_5',$login_auth['auth_5']);
			$this->session->set_userdata('auth_6',$login_auth['auth_6']);

			// SET SESSION PERSONAL DATA
			$this->session->set_userdata('logged_in',TRUE);
			$this->session->set_userdata('user_name',$test_login['user_name']);
			$this->session->set_userdata('user_auth',$login_auth['role_name']);
			$this->session->set_userdata('user_id',$test_login['user_id']);

			redirect('dashboard');

			//$this->load->library('../controllers/dashboard');
			//$this->dashboard->index();

			//require_once(APPPATH.'controllers/dashboard.php');
			//$oDashboard = new Dashboard();
			//$oDashboard->index();
			//$this->load->view('dashboard');
		}
		else{
			$error["error_message"] = "User ID atau Password salah";
			$this->load->view('login',$error);
		}
	}

	public function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect($url);
    }
}

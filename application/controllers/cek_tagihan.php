<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_tagihan extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model
        $this->load->view('default_css');
        if($this->session->userdata('logged_in')!=TRUE)
        	redirect('login');
        
        $pos=$this->deklarasi_pos();
        $pos["userdata"]=$this->session->userdata();
   		$this->load->view('default_header',$pos);
        
   	}

   	public function deklarasi_pos(){
   		$pos=array(
   			"default_pos"=>"tagihan",
   			"default_pos2"=>"cek_tagihan"
   		);
   		return $pos;
   	}

	public function index(){
		$this->load->view('cek_tagihan');
		$this->load->model('m_data_po');
	}

	public function search_result(){
		//$data = array('no_po' => $this->input->post('no_po'));
		//$data = $this->get_termin_po($data['no_po']);
		$no_po = $this->input->post('no_po');

		$sql_inb_deliv = "SELECT * FROM data_inbound_delivery WHERE purchasing_document = $no_po";

		$sql_termin = "SELECT * FROM data_termin WHERE purchasing_document = $no_po";

		$sql_denda = "SELECT * FROM data_denda WHERE purchasing_document = $no_po";

		$data = array(
			"data_inb_deliv" => $this->db->query($sql_inb_deliv)->result_array(),
			"data_termin" => $this->db->query($sql_termin)->result_array(),
			"data_denda" => $this->db->query($sql_denda)->result_array(),
			"data_turunan" => $this
		);

		//$data['data_termin'] = $this->m_data_po->GetTermin();
		//$this->load->model('m_data_po');

		//$data2 = array("call_termin" => $this->load->view("data_inb_delivery", $data));

        $this->load->view("cek_tagihan",$data);

	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_tagihan extends CI_Controller {
 	public function __construct() {
        parent::__construct();
        // load model
   		$this->load->model('m_penerimaan_tagihan');	
      $this->load->model('m_general'); 


        if($this->session->userdata('logged_in')!=TRUE)
        	redirect('login');


   		$this->load->view('default_css');
   		$pos=$this->deklarasi_pos();

        $pos["userdata"]=$this->session->userdata();
   		$this->load->view('default_header',$pos);
   	}

   	public function deklarasi_pos(){
   		$pos=array( 
   			"default_pos"=>"tagihan",
   			"default_pos2"=>"terima_tagihan"
   		);
   		return $pos;
   	}

   	public function index(){
   		$this->load->view('penerimaan_tagihan');

   	}


   	public function submit_tagihan(){
         $data = $this->input->post();

         $submit = $data["proceed"];
         $data["invoice_status"]="Received";
         if($submit=="Receive"){
            
   			    $input = $this->m_penerimaan_tagihan->save_tagihan($this->session->userdata('user_id'),$data);
   			  
            redirect('myworklist');
   		   }
         else{
            $data["invoice_status2"]="Rejected";

            if($submit=="Save & Reject")
              $this->return_tagihan($data);
            else
              $this->load->view('penerimaan_tagihan',$data);
          
         }
   	}

    public function return_tagihan($data){
      $data["reason"]="";
      //$limit = $this->m_general->get_general_value('reason_reject_number');
      //$limit = $limit["value"];
      for($i=0;$i<4;$i++){
        //declare reason position
        $x1="cbox".$i;
        //declare next reason
        $i2=$i+1;
        $x1next="cbox".$i2;
        //checking if reason is set
        if(isset($data[$x1])){
          //declare reason value box
          $x2 = "input_cbox".$i;
          //adding reasons to array
          $data["reason"].=$data[$x2];

          // Checking if there is next reason
          if(isset($data[$x1next])){
            //adding komas before next reason
            $data["reason"].=";";
          }
        }
      }

      $input = $this->m_penerimaan_tagihan->save_tagihan($this->session->userdata('user_id'),$data);
      
      if($input)
        redirect('tanda_terima');
      else
        redirect("dashboard");
    }

    public function search_vendor($no_po){
         $data["nama_vendor"]=NULL;
         $data = array(
            "no_po" => $no_po,
            "nama_vendor" => $this->m_penerimaan_tagihan->get_data_vendor($no_po),
            "invoice_status" => "Received"
         );

         return $data;
    }

   	public function search(){
   		$no_po = $this->input->post('no_po');

         $data["nama_vendor"]=NULL;
         $data = $this->search_vendor($no_po);

         $this->load->view('penerimaan_tagihan',$data);
    }
}
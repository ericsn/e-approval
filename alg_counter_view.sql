-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2019 at 11:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alg_counter_view`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_denda`
--

CREATE TABLE IF NOT EXISTS `data_denda` (
  `purchasing_document` varchar(10) NOT NULL,
  `inbound_delivery` varchar(10) NOT NULL,
  `plant` varchar(4) NOT NULL,
  `storage_location` varchar(4) NOT NULL,
  `adjustment_amount` double NOT NULL,
  `currency` varchar(3) NOT NULL,
  `status_denda` enum('Blocked','Created','Released','Done') NOT NULL,
  `created_on` date NOT NULL,
  `changed_on` date NOT NULL,
  `changed_by` varchar(32) NOT NULL,
  `alasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_denda`
--

INSERT INTO `data_denda` (`purchasing_document`, `inbound_delivery`, `plant`, `storage_location`, `adjustment_amount`, `currency`, `status_denda`, `created_on`, `changed_on`, `changed_by`, `alasan`) VALUES
('4200004822', '1200005484', '0108', '0000', 20000, 'IDR', 'Created', '2019-04-05', '0000-00-00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_inbound_delivery`
--

CREATE TABLE IF NOT EXISTS `data_inbound_delivery` (
  `purchasing_document` varchar(10) NOT NULL DEFAULT '',
  `item` int(3) NOT NULL DEFAULT '0',
  `material_description` varchar(128) DEFAULT NULL,
  `inb_delivery` varchar(10) DEFAULT NULL,
  `plant` varchar(4) DEFAULT NULL,
  `storage_location` varchar(4) DEFAULT NULL,
  `po_qty` int(2) DEFAULT NULL,
  `po_unit` varchar(5) DEFAULT NULL,
  `net_value` int(12) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `delivery_date` varchar(10) DEFAULT NULL,
  `material_document` varchar(10) NOT NULL DEFAULT '',
  `posting_date` varchar(10) DEFAULT NULL,
  `document_date` varchar(10) DEFAULT NULL,
  `qty_terima` varchar(4) DEFAULT NULL,
  `unit_terima` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_inbound_delivery`
--

INSERT INTO `data_inbound_delivery` (`purchasing_document`, `item`, `material_description`, `inb_delivery`, `plant`, `storage_location`, `po_qty`, `po_unit`, `net_value`, `currency`, `delivery_date`, `material_document`, `posting_date`, `document_date`, `qty_terima`, `unit_terima`) VALUES
('4200004800', 1, 'Pensil 2B Faber Castle', '1200005480', '0108', '0000', 200, 'PC', 500000, 'IDR', '08.04.2019', '5100000100', '06.04.2019', '06.04.2019', NULL, NULL),
('4200004807', 1, 'Spare Part AC', '1200005476', '0108', '0000', 1, 'PC', 20000000, 'IDR', '20.04.2019', '', '', '', '0.00', ''),
('4200004807', 2, 'Service AC', '', '0108', '0000', 1, 'AU', 3000000, 'IDR', '20.04.2019', '', '', '', '0.00', ''),
('4200004822', 1, 'Kranz 1 - Kursi Hadap / Meeting', '1200005484', '0108', '0000', 10, 'PC', 1000000, 'IDR', '05.04.2019', '', NULL, NULL, NULL, NULL),
('4200004822', 2, 'Kranz 1 - Kursi Kerja Kabid / Kalay\r\n', '1200005484', '0108', '0000', 2, 'PC', 200000, 'IDR', '05.04.2019', '5100000148', '05.04.2019', '04.04.2019', '2', 'PC'),
('4200004822', 3, 'Kranz 1 - Kursi Kerja Pimpinan / Wapim\r\n', '1200005484', '0108', '0000', 3, 'PC', 100000, 'IDR', '05.04.2019', '', NULL, NULL, NULL, NULL),
('4200004822', 4, 'ODNER PLASTIK 7 CM LG BCA BIRU\r\n', '1200005484', '0108', '0000', 6, 'PC', 1650000, 'IDR', '05.04.2019', '', NULL, NULL, NULL, NULL),
('4200004822', 5, 'DIVIDER 10 WARNA A4\r\n', '1200005484', '0108', '0000', 1, 'PAC', 10648000, 'IDR', '05.04.2019', '', NULL, NULL, NULL, NULL),
('4200004823', 1, 'Kalender Kerja 2019', '1200005223', '0108', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005100', '20.04.2019', '20.04.2019', '100', 'PC'),
('4200004823', 1, 'Kalender Kerja 2019', '1200005224', '0109', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005104', '20.04.2019', '20.04.2019', '100', 'PC'),
('4200004823', 1, 'Kalender Kerja 2019', '1200005225', '0110', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005105', '20.04.2019', '20.04.2019', '100', 'PC'),
('4200004823', 1, 'Kalender Kerja 2019', '1200005300', '0111', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005300', '20.04.2019', '20.04.2019', '100', 'PC'),
('4200004823', 1, 'Kalender Kerja 2019', '1200005301', '0112', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005301', '20.04.2019', '20.04.2019', '100', 'PC'),
('4200004823', 1, 'Kalender Kerja 2019', '1200005302', '0113', '0000', 100, 'PC', 300000, 'IDR', '21.04.2019', '5100005302', '20.04.2019', '20.04.2019', '100', 'PC');

-- --------------------------------------------------------

--
-- Table structure for table `data_po`
--

CREATE TABLE IF NOT EXISTS `data_po` (
  `purchasing_document` varchar(32) NOT NULL,
  `doc_date` date NOT NULL,
  `vendor` varchar(64) NOT NULL,
  `purch_group` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_po`
--

INSERT INTO `data_po` (`purchasing_document`, `doc_date`, `vendor`, `purch_group`) VALUES
('4200004807', '2019-04-10', 'Tisa Diamanta Kusuma', '982'),
('4200004822', '2019-04-05', 'Sentra Cakrawala Pusaka', '982'),
('4200004823', '2019-04-25', 'Bambi Logistik', '998');

-- --------------------------------------------------------

--
-- Table structure for table `data_termin`
--

CREATE TABLE IF NOT EXISTS `data_termin` (
  `purchasing_document` varchar(10) NOT NULL,
  `item` int(3) NOT NULL,
  `termin` varchar(20) NOT NULL,
  `invoice_percentage` double NOT NULL,
  `billing_value` int(12) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `block_status` varchar(20) NOT NULL,
  `billing_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_vendor_po`
--

CREATE TABLE IF NOT EXISTS `data_vendor_po` (
  `purchasing_document` varchar(10) NOT NULL,
  `nama_vendor` varchar(64) NOT NULL,
  `user_pembuat_po` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_vendor_po`
--

INSERT INTO `data_vendor_po` (`purchasing_document`, `nama_vendor`, `user_pembuat_po`) VALUES
('4200004807', 'Tisa Diamanta Kusuma', 'Operator Logistik'),
('4200004822', 'Sentra Cakrawala Pusaka', 'Operator Logistik'),
('4200048230', 'Bambi Logistik', 'OPR_LOG');

-- --------------------------------------------------------

--
-- Table structure for table `general_value`
--

CREATE TABLE IF NOT EXISTS `general_value` (
  `key` varchar(32) NOT NULL,
  `value` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_value`
--

INSERT INTO `general_value` (`key`, `value`) VALUES
('reason_reject_number', '4');

-- --------------------------------------------------------

--
-- Table structure for table `login_auth_details`
--

CREATE TABLE IF NOT EXISTS `login_auth_details` (
  `auth` varchar(10) NOT NULL,
  `auth_details` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_auth_details`
--

INSERT INTO `login_auth_details` (`auth`, `auth_details`) VALUES
('auth_1', 'cek_tagihan'),
('auth_2', 'terima_tagihan'),
('auth_3', 'worklist'),
('auth_4', 'tracking_tagihan'),
('auth_5', 'tanda_terima'),
('auth_6', 'register');

-- --------------------------------------------------------

--
-- Table structure for table `login_data`
--

CREATE TABLE IF NOT EXISTS `login_data` (
  `user_id` varchar(10) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_pass` varchar(128) NOT NULL,
  `login_auth` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_data`
--

INSERT INTO `login_data` (`user_id`, `user_name`, `user_pass`, `login_auth`) VALUES
('u066550', 'Dennis', '9fedcbd6ee6fe4a5d4f019d0afd297554d544578', 'laa009'),
('u066558', 'Den Den', 'ddd', 'laa000'),
('u066559', 'Tama Loy Dennis Munthe', '9fedcbd6ee6fe4a5d4f019d0afd297554d544578', 'laa001'),
('u066560', 'C. Ronaldo', '9fedcbd6ee6fe4a5d4f019d0afd297554d544578', 'laa008');

-- --------------------------------------------------------

--
-- Table structure for table `login_role_auth`
--

CREATE TABLE IF NOT EXISTS `login_role_auth` (
  `role_auth` varchar(10) NOT NULL,
  `auth_1` enum('1','0') NOT NULL,
  `auth_2` enum('1','0') NOT NULL,
  `auth_3` enum('1','0') NOT NULL,
  `auth_4` enum('1','0') NOT NULL,
  `auth_5` enum('1','0') NOT NULL,
  `auth_6` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_role_auth`
--

INSERT INTO `login_role_auth` (`role_auth`, `auth_1`, `auth_2`, `auth_3`, `auth_4`, `auth_5`, `auth_6`) VALUES
('raa000', '1', '0', '1', '1', '1', '1'),
('raa001', '1', '1', '0', '0', '1', '0'),
('raa002', '1', '0', '1', '0', '1', '0'),
('raa003', '1', '0', '1', '1', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `login_role_details`
--

CREATE TABLE IF NOT EXISTS `login_role_details` (
  `login_auth` varchar(6) NOT NULL,
  `login_name` varchar(32) NOT NULL,
  `role_auth` varchar(6) NOT NULL,
  `role_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_role_details`
--

INSERT INTO `login_role_details` (`login_auth`, `login_name`, `role_auth`, `role_name`) VALUES
('laa001', 'Loket DLOG', 'raa001', 'counter'),
('laa002', 'Biro Operasional Logistik (BOP)', 'raa002', 'gr'),
('laa003', 'Biro Operasional Logistik (BOP)', 'raa003', 'unblocktermin'),
('laa004', 'Aspek Manajemen Konstruksi (AMK)', 'raa003', 'unblocktermin'),
('laa005', 'Biro Manajemen Properti (MEP)', 'raa003', 'unblocktermin'),
('laa006', 'Biro Pengadaan Logistik (BPD)', 'raa004', 'reviewdenda'),
('laa007', 'Biro Pengadaan Gedung (PAG)', 'raa004', 'reviewdenda'),
('laa008', 'Urusan Pembayaran BPL', 'raa005', 'pembayaran'),
('laa009', 'Super Administrator', 'raa000', 'administrator'),
('laa010', 'User Administrator', 'raa000', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `masterdata_ebi`
--

CREATE TABLE IF NOT EXISTS `masterdata_ebi` (
  `purchasing_document` varchar(10) NOT NULL,
  `po_date` varchar(10) NOT NULL,
  `vendor` varchar(64) NOT NULL,
  `purchasing_group` varchar(3) NOT NULL,
  `gr_based` tinyint(1) NOT NULL,
  `invoicing_plan` tinyint(1) NOT NULL,
  `inbound_delivery` varchar(10) NOT NULL,
  `plant` varchar(4) NOT NULL,
  `storage_location` varchar(4) NOT NULL,
  `currency` varchar(4) NOT NULL,
  `delivery_date` varchar(10) NOT NULL,
  `material_document` varchar(10) NOT NULL,
  `document_date` varchar(10) NOT NULL,
  `full_gr` tinyint(1) NOT NULL,
  `termin` int(2) NOT NULL,
  `invoice_percentage` varchar(6) NOT NULL,
  `block_status` enum('','Blocked for Invoice','Unblock Invoice') NOT NULL,
  `adjustment_amount` double NOT NULL,
  `status_denda` enum('','Created','Blocked','Released','Done') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterdata_ebi`
--

INSERT INTO `masterdata_ebi` (`purchasing_document`, `po_date`, `vendor`, `purchasing_group`, `gr_based`, `invoicing_plan`, `inbound_delivery`, `plant`, `storage_location`, `currency`, `delivery_date`, `material_document`, `document_date`, `full_gr`, `termin`, `invoice_percentage`, `block_status`, `adjustment_amount`, `status_denda`) VALUES
('4200004822', '20.03.2019', 'Sentra Cakrawala Pusaka', '982', 1, 0, '1200005484', '0108', '0000', 'IDR', '05.04.2019', '5100000148', '06.04.2019', 1, 0, '', '', 100, 'Created'),
('4200004822', '20.03.2019', 'Sentra Cakrawala Pusaka', '982', 1, 0, '1200005485', '0109', '0000', 'IDR', '05.04.2019', '5100000149', '06.04.2019', 1, 0, '', '', 150, 'Created'),
('4200004822', '20.03.2019', 'Sentra Cakrawala Pusaka', '982', 1, 0, '1200005486', '0110', '0000', 'IDR', '05.04.2019', '5100000150', '06.04.2019', 1, 0, '', '', 200, 'Created');

-- --------------------------------------------------------

--
-- Table structure for table `masterdata_invoice`
--

CREATE TABLE IF NOT EXISTS `masterdata_invoice` (
  `purchasing_document` varchar(10) NOT NULL,
  `invoice_number` varchar(32) NOT NULL,
  `id` varchar(512) NOT NULL,
  `tanggal_invoice` date NOT NULL,
  `no_antrian` int(3) NOT NULL,
  `tanggal_terima` date NOT NULL,
  `total_invoice` varchar(16) NOT NULL,
  `mata_uang` varchar(3) NOT NULL,
  `jenis_tagihan` varchar(64) NOT NULL,
  `untuk_tagihan` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterdata_invoice`
--

INSERT INTO `masterdata_invoice` (`purchasing_document`, `invoice_number`, `id`, `tanggal_invoice`, `no_antrian`, `tanggal_terima`, `total_invoice`, `mata_uang`, `jenis_tagihan`, `untuk_tagihan`) VALUES
('4200004822', '', '', '0000-00-00', 0, '0000-00-00', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `masterdata_invoice_log`
--

CREATE TABLE IF NOT EXISTS `masterdata_invoice_log` (
  `purchasing_document` varchar(10) NOT NULL,
  `invoice_repeat` int(3) NOT NULL,
  `invoice_number` varchar(32) NOT NULL,
  `post` enum('counter','gr','unblocktermin','reviewdenda','pembayaran') NOT NULL,
  `pic_post` varchar(10) NOT NULL,
  `status_post` enum('','Processing','Accepted','Rejected','Sent','Returned','Done') NOT NULL,
  `reason` varchar(64) NOT NULL,
  `status_post_date_in` date NOT NULL,
  `status_post_date_out` date NOT NULL,
  `status_post_time_in` time NOT NULL,
  `status_post_time_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterdata_invoice_log`
--

INSERT INTO `masterdata_invoice_log` (`purchasing_document`, `invoice_repeat`, `invoice_number`, `post`, `pic_post`, `status_post`, `reason`, `status_post_date_in`, `status_post_date_out`, `status_post_time_in`, `status_post_time_out`) VALUES
('4200004822', 1, '', 'counter', 'u066559', 'Accepted', '', '0000-00-00', '0000-00-00', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tanda_terima_invoice`
--

CREATE TABLE IF NOT EXISTS `tanda_terima_invoice` (
  `purchasing_document` varchar(10) NOT NULL,
  `invoice_repeat` int(2) NOT NULL,
  `invoice_number` varchar(32) NOT NULL,
  `invoice_status` enum('','Rejected','Received') NOT NULL,
  `invoice_status_date` date NOT NULL,
  `invoice_status_reason` varchar(64) NOT NULL,
  `id` varchar(512) NOT NULL,
  `pic_loket` varchar(10) NOT NULL,
  `status_loket` enum('','Processing','Sent','Returned') NOT NULL,
  `status_loket_date_awal` date NOT NULL,
  `status_loket_date_akhir` date NOT NULL,
  `pic_gr` varchar(10) NOT NULL,
  `status_gr` enum('','Accepted','Rejected','Sent','Returned') NOT NULL,
  `status_gr_date_awal` date NOT NULL,
  `status_gr_date_akhir` date NOT NULL,
  `pic_unblocktermin` varchar(10) NOT NULL,
  `status_unblocktermin` enum('','Accepted','Rejected','Sent','Returned') NOT NULL,
  `status_unblocktermin_date_awal` date NOT NULL,
  `status_unblocktermin_date_akhir` date NOT NULL,
  `pic_reviewdenda` varchar(10) NOT NULL,
  `status_reviewdenda` enum('','Accepted','Rejected','Sent','Returned') NOT NULL,
  `status_reviewdenda_date_awal` date NOT NULL,
  `status_reviewdenda_date_akhir` date NOT NULL,
  `pic_pembayaran` varchar(10) NOT NULL,
  `status_pembayaran` enum('','Accepted','Rejected','Done') NOT NULL,
  `status_pembayaran_date_awal` date NOT NULL,
  `status_pembayaran_date_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanda_terima_invoice`
--

INSERT INTO `tanda_terima_invoice` (`purchasing_document`, `invoice_repeat`, `invoice_number`, `invoice_status`, `invoice_status_date`, `invoice_status_reason`, `id`, `pic_loket`, `status_loket`, `status_loket_date_awal`, `status_loket_date_akhir`, `pic_gr`, `status_gr`, `status_gr_date_awal`, `status_gr_date_akhir`, `pic_unblocktermin`, `status_unblocktermin`, `status_unblocktermin_date_awal`, `status_unblocktermin_date_akhir`, `pic_reviewdenda`, `status_reviewdenda`, `status_reviewdenda_date_awal`, `status_reviewdenda_date_akhir`, `pic_pembayaran`, `status_pembayaran`, `status_pembayaran_date_awal`, `status_pembayaran_date_akhir`) VALUES
('4200004822', 1, 'A011', 'Rejected', '0000-00-00', 'Kurang Dokumen Syarat BayarKurang Data Surat Jalan/Tanda Terima', '', 'u066559', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00'),
('4200004822', 1, 'A012', 'Received', '2019-05-05', '', '', 'u066559', 'Processing', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00'),
('4200004822', 1, 'A013', 'Received', '0000-00-00', '', '1200005522;', 'u066559', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00'),
('4200004823', 1, 'INV-BAMLOG-25.04.2019.0003', 'Received', '0000-00-00', '', '', 'u066559', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tanda_terima_invoice_log`
--

CREATE TABLE IF NOT EXISTS `tanda_terima_invoice_log` (
  `purchasing_document` varchar(32) NOT NULL,
  `invoice_number` varchar(32) NOT NULL,
  `pic_post` enum('loket','gr','unblocktermin','reviewdenda','pembayaran') NOT NULL,
  `pic` varchar(10) NOT NULL,
  `pic_status` varchar(10) NOT NULL,
  `pic_status_date_awal` date NOT NULL,
  `pic_status_date_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `termin-contoh`
--

CREATE TABLE IF NOT EXISTS `termin-contoh` (
  `Purchasing Document` varchar(10) NOT NULL,
  `Item` int(3) NOT NULL,
  `Material Desc` varchar(128) NOT NULL,
  `Inb Delivery` varchar(10) NOT NULL,
  `Plant` varchar(4) NOT NULL,
  `Storage Location` varchar(4) NOT NULL,
  `PO Qty` int(3) NOT NULL,
  `Unit` enum('PC','AU') NOT NULL,
  `Net Value` int(10) NOT NULL,
  `Currency` varchar(4) NOT NULL,
  `Delivery Date` date NOT NULL,
  `Material Document` varchar(10) NOT NULL,
  `Posting Date` date NOT NULL,
  `Document Date` date NOT NULL,
  `Qty Terima` int(3) NOT NULL,
  `Unit Terima` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
  `nim` varchar(9) NOT NULL,
  `name` varchar(64) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mail` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`nim`, `name`, `gender`, `dob`, `phone`, `mail`) VALUES
('141402100', 'Tama Loy Dennis Munthe', 'Male', '1996-06-14', '082167471945', 'dennis141402100@gmail.com\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `nim_user` varchar(9) NOT NULL,
  `password` varchar(32) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `special_privilege` enum('aslab','praktikan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`nim_user`, `password`, `last_login`, `special_privilege`) VALUES
('141402100', 'dennismunthe', '0000-00-00 00:00:00', 'aslab');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_login_details`
--
CREATE TABLE IF NOT EXISTS `v_login_details` (
`user_id` varchar(10)
,`login_auth` varchar(6)
,`login_name` varchar(32)
,`role_name` varchar(32)
,`role_auth` varchar(10)
,`auth_1` enum('1','0')
,`auth_2` enum('1','0')
,`auth_3` enum('1','0')
,`auth_4` enum('1','0')
,`auth_5` enum('1','0')
,`auth_6` enum('1','0')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_masterdata_invoice`
--
CREATE TABLE IF NOT EXISTS `v_masterdata_invoice` (
`purchasing_document` varchar(10)
,`invoice_number` varchar(32)
,`id` varchar(512)
,`pic_loket` varchar(10)
,`status_loket` enum('','Processing','Accepted','Rejected','Sent','Returned','Done')
,`status_loket_reason` varchar(64)
,`status_loket_date_in` date
,`status_loket_date_out` date
,`status_loket_time_in` date
,`status_loket_time_out` date
,`DD_loket` int(7)
,`SLA_loket` int(7)
,`pic_gr` varchar(10)
,`status_gr` enum('','Processing','Accepted','Rejected','Sent','Returned','Done')
,`status_gr_reason` varchar(64)
,`status_gr_date_in` date
,`status_gr_date_out` date
,`status_gr_time_in` date
,`status_gr_time_out` date
,`DD_gr` int(7)
,`SLA_gr` int(7)
,`pic_unblocktermin` varchar(10)
,`status_unblocktermin` enum('','Processing','Accepted','Rejected','Sent','Returned','Done')
,`status_unblocktermin_reason` varchar(64)
,`status_unblocktermin_date_in` date
,`status_unblocktermin_date_out` date
,`status_unblocktermin_time_in` date
,`status_unblocktermin_time_out` date
,`DD_unblocktermin` int(7)
,`SLA_unblocktermin` int(7)
,`pic_reviewdenda` varchar(10)
,`status_reviewdenda` enum('','Processing','Accepted','Rejected','Sent','Returned','Done')
,`status_reviewdenda_reason` varchar(64)
,`status_reviewdenda_date_in` date
,`status_reviewdenda_date_out` date
,`status_reviewdenda_time_in` date
,`status_reviewdenda_time_out` date
,`DD_reviewdenda` int(7)
,`SLA_reviewdenda` int(7)
,`pic_pembayaran` varchar(10)
,`status_pembayaran` enum('','Processing','Accepted','Rejected','Sent','Returned','Done')
,`status_pembayaran_reason` varchar(64)
,`status_pembayaran_date_in` date
,`status_pembayaran_date_out` date
,`status_pembayaran_time_in` date
,`status_pembayaran_time_out` date
,`DD_pembayaran` int(7)
,`SLA_pembayaran` int(7)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_tanda_terima_invoice`
--
CREATE TABLE IF NOT EXISTS `v_tanda_terima_invoice` (
`purchasing_document` varchar(10)
,`invoice_repeat` int(2)
,`invoice_number` varchar(32)
,`invoice_status` enum('','Rejected','Received')
,`invoice_status_date` date
,`invoice_status_reason` varchar(64)
,`id` varchar(512)
,`pic_loket` varchar(10)
,`status_loket` enum('','Processing','Sent','Returned')
,`status_loket_date_awal` date
,`status_loket_date_akhir` date
,`pic_gr` varchar(10)
,`status_gr` enum('','Accepted','Rejected','Sent','Returned')
,`status_gr_date_awal` date
,`status_gr_date_akhir` date
,`pic_unblocktermin` varchar(10)
,`status_unblocktermin` enum('','Accepted','Rejected','Sent','Returned')
,`status_unblocktermin_date_awal` date
,`status_unblocktermin_date_akhir` date
,`pic_reviewdenda` varchar(10)
,`status_reviewdenda` enum('','Accepted','Rejected','Sent','Returned')
,`status_reviewdenda_date_awal` date
,`status_reviewdenda_date_akhir` date
,`pic_pembayaran` varchar(10)
,`status_pembayaran` enum('','Accepted','Rejected','Done')
,`status_pembayaran_date_awal` date
,`status_pembayaran_date_akhir` date
,`DD_loket` int(7)
,`DD_gr` int(7)
,`DD_unblocktermin` int(7)
,`DD_reviewdenda` int(7)
,`DD_pembayaran` int(7)
,`SLA_loket` int(7)
,`SLA_gr` int(7)
,`SLA_unblocktermin` int(7)
,`SLA_reviewdenda` int(7)
,`SLA_pembayaran` int(7)
);
-- --------------------------------------------------------

--
-- Structure for view `v_login_details`
--
DROP TABLE IF EXISTS `v_login_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_login_details` AS select `a`.`user_id` AS `user_id`,`b`.`login_auth` AS `login_auth`,`b`.`login_name` AS `login_name`,`b`.`role_name` AS `role_name`,`c`.`role_auth` AS `role_auth`,`c`.`auth_1` AS `auth_1`,`c`.`auth_2` AS `auth_2`,`c`.`auth_3` AS `auth_3`,`c`.`auth_4` AS `auth_4`,`c`.`auth_5` AS `auth_5`,`c`.`auth_6` AS `auth_6` from ((`login_data` `a` join `login_role_details` `b`) join `login_role_auth` `c`) where ((`a`.`login_auth` = `b`.`login_auth`) and (`b`.`role_auth` = `c`.`role_auth`));

-- --------------------------------------------------------

--
-- Structure for view `v_masterdata_invoice`
--
DROP TABLE IF EXISTS `v_masterdata_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_masterdata_invoice` AS select `a`.`purchasing_document` AS `purchasing_document`,`a`.`invoice_number` AS `invoice_number`,`a`.`id` AS `id`,`b`.`pic_post` AS `pic_loket`,`b`.`status_post` AS `status_loket`,`b`.`reason` AS `status_loket_reason`,`b`.`status_post_date_in` AS `status_loket_date_in`,`b`.`status_post_date_in` AS `status_loket_date_out`,`b`.`status_post_date_in` AS `status_loket_time_in`,`b`.`status_post_date_in` AS `status_loket_time_out`,(to_days(`b`.`status_post_date_out`) - to_days(`b`.`status_post_date_in`)) AS `DD_loket`,(to_days(curdate()) - to_days(`b`.`status_post_date_in`)) AS `SLA_loket`,`c`.`pic_post` AS `pic_gr`,`c`.`status_post` AS `status_gr`,`c`.`reason` AS `status_gr_reason`,`c`.`status_post_date_in` AS `status_gr_date_in`,`c`.`status_post_date_in` AS `status_gr_date_out`,`c`.`status_post_date_in` AS `status_gr_time_in`,`c`.`status_post_date_in` AS `status_gr_time_out`,(to_days(`c`.`status_post_date_out`) - to_days(`c`.`status_post_date_in`)) AS `DD_gr`,(to_days(curdate()) - to_days(`c`.`status_post_date_in`)) AS `SLA_gr`,`d`.`pic_post` AS `pic_unblocktermin`,`d`.`status_post` AS `status_unblocktermin`,`d`.`reason` AS `status_unblocktermin_reason`,`d`.`status_post_date_in` AS `status_unblocktermin_date_in`,`d`.`status_post_date_in` AS `status_unblocktermin_date_out`,`d`.`status_post_date_in` AS `status_unblocktermin_time_in`,`d`.`status_post_date_in` AS `status_unblocktermin_time_out`,(to_days(`d`.`status_post_date_out`) - to_days(`d`.`status_post_date_in`)) AS `DD_unblocktermin`,(to_days(curdate()) - to_days(`d`.`status_post_date_in`)) AS `SLA_unblocktermin`,`e`.`pic_post` AS `pic_reviewdenda`,`e`.`status_post` AS `status_reviewdenda`,`e`.`reason` AS `status_reviewdenda_reason`,`e`.`status_post_date_in` AS `status_reviewdenda_date_in`,`e`.`status_post_date_in` AS `status_reviewdenda_date_out`,`e`.`status_post_date_in` AS `status_reviewdenda_time_in`,`e`.`status_post_date_in` AS `status_reviewdenda_time_out`,(to_days(`e`.`status_post_date_out`) - to_days(`e`.`status_post_date_in`)) AS `DD_reviewdenda`,(to_days(curdate()) - to_days(`e`.`status_post_date_in`)) AS `SLA_reviewdenda`,`f`.`pic_post` AS `pic_pembayaran`,`f`.`status_post` AS `status_pembayaran`,`f`.`reason` AS `status_pembayaran_reason`,`f`.`status_post_date_in` AS `status_pembayaran_date_in`,`f`.`status_post_date_in` AS `status_pembayaran_date_out`,`f`.`status_post_date_in` AS `status_pembayaran_time_in`,`f`.`status_post_date_in` AS `status_pembayaran_time_out`,(to_days(`f`.`status_post_date_out`) - to_days(`f`.`status_post_date_in`)) AS `DD_pembayaran`,(to_days(curdate()) - to_days(`f`.`status_post_date_in`)) AS `SLA_pembayaran` from (((((`masterdata_invoice` `a` left join `masterdata_invoice_log` `b` on((`b`.`post` = 'counter'))) left join `masterdata_invoice_log` `c` on((`c`.`post` = 'gr'))) left join `masterdata_invoice_log` `d` on((`d`.`post` = 'unblcoktermin'))) left join `masterdata_invoice_log` `e` on((`e`.`post` = 'reviewdenda'))) left join `masterdata_invoice_log` `f` on((`f`.`post` = 'pembayaran'))) group by `a`.`invoice_number`;

-- --------------------------------------------------------

--
-- Structure for view `v_tanda_terima_invoice`
--
DROP TABLE IF EXISTS `v_tanda_terima_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tanda_terima_invoice` AS select `tanda_terima_invoice`.`purchasing_document` AS `purchasing_document`,`tanda_terima_invoice`.`invoice_repeat` AS `invoice_repeat`,`tanda_terima_invoice`.`invoice_number` AS `invoice_number`,`tanda_terima_invoice`.`invoice_status` AS `invoice_status`,`tanda_terima_invoice`.`invoice_status_date` AS `invoice_status_date`,`tanda_terima_invoice`.`invoice_status_reason` AS `invoice_status_reason`,`tanda_terima_invoice`.`id` AS `id`,`tanda_terima_invoice`.`pic_loket` AS `pic_loket`,`tanda_terima_invoice`.`status_loket` AS `status_loket`,`tanda_terima_invoice`.`status_loket_date_awal` AS `status_loket_date_awal`,`tanda_terima_invoice`.`status_loket_date_akhir` AS `status_loket_date_akhir`,`tanda_terima_invoice`.`pic_gr` AS `pic_gr`,`tanda_terima_invoice`.`status_gr` AS `status_gr`,`tanda_terima_invoice`.`status_gr_date_awal` AS `status_gr_date_awal`,`tanda_terima_invoice`.`status_gr_date_akhir` AS `status_gr_date_akhir`,`tanda_terima_invoice`.`pic_unblocktermin` AS `pic_unblocktermin`,`tanda_terima_invoice`.`status_unblocktermin` AS `status_unblocktermin`,`tanda_terima_invoice`.`status_unblocktermin_date_awal` AS `status_unblocktermin_date_awal`,`tanda_terima_invoice`.`status_unblocktermin_date_akhir` AS `status_unblocktermin_date_akhir`,`tanda_terima_invoice`.`pic_reviewdenda` AS `pic_reviewdenda`,`tanda_terima_invoice`.`status_reviewdenda` AS `status_reviewdenda`,`tanda_terima_invoice`.`status_reviewdenda_date_awal` AS `status_reviewdenda_date_awal`,`tanda_terima_invoice`.`status_reviewdenda_date_akhir` AS `status_reviewdenda_date_akhir`,`tanda_terima_invoice`.`pic_pembayaran` AS `pic_pembayaran`,`tanda_terima_invoice`.`status_pembayaran` AS `status_pembayaran`,`tanda_terima_invoice`.`status_pembayaran_date_awal` AS `status_pembayaran_date_awal`,`tanda_terima_invoice`.`status_pembayaran_date_akhir` AS `status_pembayaran_date_akhir`,(to_days(`tanda_terima_invoice`.`status_loket_date_akhir`) - to_days(`tanda_terima_invoice`.`status_loket_date_awal`)) AS `DD_loket`,(to_days(`tanda_terima_invoice`.`status_gr_date_akhir`) - to_days(`tanda_terima_invoice`.`status_gr_date_awal`)) AS `DD_gr`,(to_days(`tanda_terima_invoice`.`status_unblocktermin_date_akhir`) - to_days(`tanda_terima_invoice`.`status_unblocktermin_date_awal`)) AS `DD_unblocktermin`,(to_days(`tanda_terima_invoice`.`status_reviewdenda_date_akhir`) - to_days(`tanda_terima_invoice`.`status_reviewdenda_date_awal`)) AS `DD_reviewdenda`,(to_days(`tanda_terima_invoice`.`status_pembayaran_date_akhir`) - to_days(`tanda_terima_invoice`.`status_pembayaran_date_awal`)) AS `DD_pembayaran`,(to_days(curdate()) - to_days(`tanda_terima_invoice`.`status_loket_date_awal`)) AS `SLA_loket`,(to_days(curdate()) - to_days(`tanda_terima_invoice`.`status_gr_date_awal`)) AS `SLA_gr`,(to_days(curdate()) - to_days(`tanda_terima_invoice`.`status_unblocktermin_date_awal`)) AS `SLA_unblocktermin`,(to_days(curdate()) - to_days(`tanda_terima_invoice`.`status_reviewdenda_date_awal`)) AS `SLA_reviewdenda`,(to_days(curdate()) - to_days(`tanda_terima_invoice`.`status_pembayaran_date_awal`)) AS `SLA_pembayaran` from `tanda_terima_invoice`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_inbound_delivery`
--
ALTER TABLE `data_inbound_delivery`
 ADD PRIMARY KEY (`purchasing_document`,`item`,`material_document`);

--
-- Indexes for table `data_po`
--
ALTER TABLE `data_po`
 ADD PRIMARY KEY (`purchasing_document`);

--
-- Indexes for table `data_vendor_po`
--
ALTER TABLE `data_vendor_po`
 ADD PRIMARY KEY (`purchasing_document`);

--
-- Indexes for table `general_value`
--
ALTER TABLE `general_value`
 ADD PRIMARY KEY (`key`);

--
-- Indexes for table `login_data`
--
ALTER TABLE `login_data`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `login_role_auth`
--
ALTER TABLE `login_role_auth`
 ADD PRIMARY KEY (`role_auth`);

--
-- Indexes for table `login_role_details`
--
ALTER TABLE `login_role_details`
 ADD PRIMARY KEY (`login_auth`,`role_auth`);

--
-- Indexes for table `masterdata_ebi`
--
ALTER TABLE `masterdata_ebi`
 ADD PRIMARY KEY (`purchasing_document`,`inbound_delivery`,`termin`);

--
-- Indexes for table `masterdata_invoice`
--
ALTER TABLE `masterdata_invoice`
 ADD PRIMARY KEY (`purchasing_document`,`invoice_number`);

--
-- Indexes for table `masterdata_invoice_log`
--
ALTER TABLE `masterdata_invoice_log`
 ADD PRIMARY KEY (`purchasing_document`,`invoice_repeat`,`invoice_number`,`post`,`status_post`);

--
-- Indexes for table `tanda_terima_invoice`
--
ALTER TABLE `tanda_terima_invoice`
 ADD PRIMARY KEY (`purchasing_document`,`invoice_repeat`,`invoice_number`,`invoice_status`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
 ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
 ADD PRIMARY KEY (`nim_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
